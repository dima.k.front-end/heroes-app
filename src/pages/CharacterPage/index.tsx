import { useParams } from 'react-router-dom';
import { useEffect } from 'react';

import { AdvertisingBanner } from 'components/ComicsPage/AdvertisingBanner';
import { Loader } from 'components/Loader';
import { Image } from 'components/Image';
import { useAppDispatch, useAppSelector } from 'hooks/redux-hook';
import { useMediaQuery } from 'hooks/useMediaQuery';
import { Status } from 'utils/const';
import { device } from 'utils/device';
import { character } from 'store/characters/character';
import { Wrap, Name, Description, ImageWrap } from './styled';

function CharacterPage() {
  const dispatch = useAppDispatch();
  const { id } = useParams<{ id: string }>();
  const data = useAppSelector(character.selectors.character);
  const fetchingStatus = useAppSelector(
    character.selectors.characterFetchingStatus
  );
  const loading =
    fetchingStatus === Status.IDLE || fetchingStatus === Status.PENDING;

  const isMobileDevice = useMediaQuery(device.md);

  useEffect(() => {
    dispatch(character.thunks.getCharacterById(id));
  }, []);

  const content = loading ? (
    <Loader />
  ) : !data ? null : (
    <>
      <ImageWrap>
        <Image
          src={data.imageHref}
          alt={data.name}
        />
      </ImageWrap>
      <div>
        <Name>{data.name}</Name>
        <Description>{data.description}</Description>
      </div>
    </>
  );

  return (
    <>
      {!isMobileDevice && (
        <AdvertisingBanner css={{ width: '100%', height: '100px' }} />
      )}
      <Wrap>{content}</Wrap>
    </>
  );
}

export default CharacterPage;
