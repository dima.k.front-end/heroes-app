import { Button } from 'components/Form/Button';
import { Wrap } from './styled';

function NotFound() {
  return (
    <Wrap>
      This page not found
      <Button
        to="/characters"
        css={{ fontSize: '18px' }}
      >
        Back to main page
      </Button>
    </Wrap>
  );
}

export default NotFound;
