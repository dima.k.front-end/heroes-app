import styled from 'styled-components';

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: 30px;
  justify-content: center;
  height: 100%;
  font-size: 32px;
  font-weight: 700;
  text-transform: uppercase;
`;

export { Wrap };
