import styled from 'styled-components';

import { device } from 'utils/device';

const Wrap = styled.div`
  width: 100%;
`;

const ItemsWrap = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 30px;
  min-height: 860px;
  margin-top: 50px;

  @media ${device.md} {
    margin-top: 0;
    grid-template-columns: repeat(3, 1fr);
  }

  @media ${device.sm} {
    min-height: 400px;
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${device.xsm} {
    grid-template-columns: 1fr;
  }
`;

export { Wrap, ItemsWrap };
