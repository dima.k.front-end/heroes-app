import { IThumbnail, IUrl } from 'types/common';

interface ComicsItem {
  resourceURI: string;
  name: string;
}

interface Comics {
  available: number;
  collectionURI: string;
  items: ComicsItem[];
  returned: number;
}

interface ICharacterOrig {
  id: number;
  name: string;
  description: string;
  modified: Date;
  thumbnail: IThumbnail;
  resourceURI: string;
  comics: Comics;
  urls: IUrl[];
}

interface ICharacterCustom {
  id: number;
  name: string;
  imageHref: string;
  description: string;
  wikiUrl?: string;
}

export type { ICharacterOrig, ICharacterCustom };
