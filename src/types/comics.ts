import { IThumbnail, IUrl } from 'types/common';

interface ISeries {
  resourceURI: string;
  name: string;
}

interface IVariant {
  resourceURI: string;
  name: string;
}

interface IDate {
  type: string;
  date: Date;
}

interface IPrice {
  type: string;
  price: number;
}

interface ICreators {
  available: number;
  collectionURI: string;
  items: any[];
  returned: number;
}

interface ICharacters {
  available: number;
  collectionURI: string;
  items: any[];
  returned: number;
}

interface IStories {
  available: number;
  collectionURI: string;
  items: any[];
  returned: number;
}

interface IEvents {
  available: number;
  collectionURI: string;
  items: any[];
  returned: number;
}

interface IComicsOrig {
  id: number;
  digitalId: number;
  title: string;
  issueNumber: number;
  variantDescription: string;
  description: string;
  modified: Date;
  isbn: string;
  upc: string;
  diamondCode: string;
  ean: string;
  issn: string;
  format: string;
  pageCount: number;
  textObjects: any[];
  resourceURI: string;
  urls: IUrl[];
  series: ISeries;
  variants: IVariant[];
  collections: any[];
  collectedIssues: any[];
  dates: IDate[];
  prices: IPrice[];
  thumbnail: IThumbnail;
  images: any[];
  creators: ICreators;
  characters: ICharacters;
  stories: IStories;
  events: IEvents;
}

interface IComicsCustom {
  id: number;
  title: string;
  description: string;
  imageHref: string;
  price: string;
  pageCount: number;
  language: string;
  wikiUrl?: string;
}

export type { IComicsOrig, IComicsCustom };
