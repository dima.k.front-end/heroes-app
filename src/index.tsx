import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Theme from 'components/Theme';
import App from 'components/App';
import { GlobalStyles } from 'components/Theme/GlobalStyles';
import store from 'store';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <Theme>
        <GlobalStyles />
        <App />
      </Theme>
    </BrowserRouter>
    <ToastContainer />
  </Provider>
);
