import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://gateway.marvel.com/v1/public',
  params: {
    ts: process.env.REACT_APP_TS,
    apikey: process.env.REACT_APP_PUBLIC_KEY,
    hash: process.env.REACT_APP_HASH,
  },
});

export default instance;
