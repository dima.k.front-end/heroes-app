const objWithoutEmptyProp = (obj: Record<string, any>) => {
  return Object.fromEntries(Object.entries(obj).filter((item) => item[1]));
};

export { objWithoutEmptyProp };
