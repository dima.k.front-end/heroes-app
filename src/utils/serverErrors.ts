import { toast } from 'react-toastify';
import { AxiosError, AxiosResponse } from 'axios';

export const handleServerErrors = (error: AxiosError) => {
  const { data, status, statusText } = error.response as AxiosResponse;

  if (data && data.status) {
    toast.error(data.status);
  } else {
    toast.error(`Error ${status}: ${statusText}`);
  }
};
