import { RootState } from 'store';

export const selectors = {
  list: (state: RootState) => state.characters.list,
  pagination: (state: RootState) => state.characters.pagination,
  fetchingStatus: (state: RootState) => state.characters.fetchingStatus,
};
