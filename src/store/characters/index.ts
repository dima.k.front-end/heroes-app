import { createSlice } from '@reduxjs/toolkit';

import { characterModel } from 'models/character';
import { pagination } from 'models/pagination';
import { Status } from 'utils/const';
import { IPagination } from 'types/common';
import { ICharacterCustom } from 'types/characters';
import { selectors } from './selectors';
import { thunks } from './thunks';

interface IState {
  list: ICharacterCustom[];
  pagination: IPagination;
  fetchingStatus: Status;
}

const initialState: IState = {
  list: [],
  pagination: pagination({ perPage: 6 }),
  fetchingStatus: Status.IDLE,
};

const slice = createSlice({
  name: 'characters',
  initialState,
  reducers: {
    SET_PAGINATION: (state, { payload }) => {
      state.pagination.current = payload.page;
      state.pagination.offset = payload.offset;
    },
  },
  extraReducers: (builder) =>
    builder
      .addCase(thunks.getAllCharacters.pending, (state) => {
        state.fetchingStatus = Status.PENDING;
      })
      .addCase(thunks.getAllCharacters.fulfilled, (state, { payload }) => {
        if (payload) {
          const { results, offset, total, limit } = payload;
          state.list = results.map((character) => characterModel(character));
          state.pagination = pagination({ total, perPage: limit, offset });
        }
        state.fetchingStatus = Status.SUCCESS;
      })
      .addCase(thunks.getAllCharacters.rejected, (state) => {
        state.fetchingStatus = Status.FAIL;
      }),
});

export const characters = {
  actions: slice.actions,
  selectors,
  thunks,
};
export default slice.reducer;
