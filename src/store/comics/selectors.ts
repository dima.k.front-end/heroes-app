import { RootState } from 'store';

export const selectors = {
  list: (state: RootState) => state.comics.list,
  pagination: (state: RootState) => state.comics.pagination,
  filters: (state: RootState) => state.comics.filters,
  fetchingStatus: (state: RootState) => state.comics.fetchingStatus,
};
