import { RootState } from 'store';

export const selectors = {
  data: (state: RootState) => state.comic.data,
  fetchingStatus: (state: RootState) => state.comic.fetchingStatus,
};
