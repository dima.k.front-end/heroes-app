import { createSlice } from '@reduxjs/toolkit';

import { comicsModel } from 'models/comics';
import { ImageSize, Status } from 'utils/const';
import { IComicsCustom } from 'types/comics';
import { selectors } from './selectors';
import { thunks } from './thunks';

interface IState {
  data: IComicsCustom | null;
  fetchingStatus: Status;
}

const initialState: IState = {
  data: null,
  fetchingStatus: Status.IDLE,
};

const slice = createSlice({
  name: 'comic',
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) =>
    builder
      .addCase(thunks.getComicById.pending, (state) => {
        state.fetchingStatus = Status.PENDING;
      })
      .addCase(thunks.getComicById.fulfilled, (state, { payload }) => {
        if (payload) {
          state.data = comicsModel(payload, ImageSize.PORTRAIT_UNCANNY);
        }
        state.fetchingStatus = Status.SUCCESS;
      })
      .addCase(thunks.getComicById.rejected, (state) => {
        state.fetchingStatus = Status.FAIL;
      }),
});

export const comic = {
  actions: slice.actions,
  selectors,
  thunks,
};
export default slice.reducer;
