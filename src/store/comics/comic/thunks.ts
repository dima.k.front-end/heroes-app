import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';

import api from 'api';
import { handleServerErrors } from 'utils/serverErrors';
import { IComicsOrig } from 'types/comics';

const getComicById = createAsyncThunk(
  'comics/getComicById',
  async (id: number | string) => {
    try {
      const { data } = await api.comics.getComicById(id);
      return data.data.results[0] as IComicsOrig;
    } catch (err) {
      if (err instanceof AxiosError) {
        handleServerErrors(err);
        throw err;
      }
    }
  }
);

export const thunks = {
  getComicById,
};
