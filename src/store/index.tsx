import { combineReducers, configureStore } from '@reduxjs/toolkit';

import characters from './characters';
import character from './characters/character';
import comics from './comics';
import comic from './comics/comic';

const rootReducer = combineReducers({
  characters,
  character,
  comics,
  comic,
});

const store = configureStore({ reducer: rootReducer, devTools: true });

export type RootState = ReturnType<typeof store.getState>;
export default store;
