import { useMediaQuery } from 'hooks/useMediaQuery';
import { device } from 'utils/device';
import { Logo, Wrap, LinksBlock, Link } from './styled';

export function Header() {
  const isMobileDevice = useMediaQuery(device.md);

  return (
    <Wrap>
      <Logo>
        {isMobileDevice ? (
          <>
            <span>M</span>P
          </>
        ) : (
          <>
            <span>Marvel</span> information portal
          </>
        )}
      </Logo>
      <LinksBlock>
        <Link to="/characters">Characters</Link>
        <span>/</span>
        <Link to="/comics">Comics</Link>
      </LinksBlock>
    </Wrap>
  );
}
