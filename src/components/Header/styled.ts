import styled, { css } from 'styled-components';
import { NavLink } from 'react-router-dom';

import { device } from 'utils/device';

const Wrap = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 50px;

  @media ${device.lg} {
    margin-bottom: 30px;
  }
`;

const Logo = styled.div`
  font-size: 28px;
  font-weight: 700;
  color: ${({ theme }) => theme.black};

  & > span {
    color: ${({ theme }) => theme.red};
  }
`;

const LinksBlock = styled.div`
  font-weight: 700;
  font-size: 24px;

  & > span {
    margin: 0 8px;
  }
`;

const Link = styled(NavLink)`
  ${({ theme }) => css`
    color: ${theme.black};

    &.active {
      color: ${theme.red};
    }
  `}
`;

export { Wrap, Logo, LinksBlock, Link };
