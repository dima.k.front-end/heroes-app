import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;

    &::-webkit-scrollbar {
      height: 5px;
      width: 5px;
    }

    &::-webkit-scrollbar-track {
      background: ${({ theme }) => theme.white};
      border-radius: 5px
    }

    &::-webkit-scrollbar-thumb {
      background: ${({ theme }) => theme.red};
      border-radius: 5px;
    }
  }

  html, body, #root {
    height: 100%;
  }
  
  body {
    font-family: "Roboto Condensed", sans-serif;
    line-height: 114%;
  }
  
  a {
    text-decoration: none;
  }
  
  .Toastify__toast--error {
    .Toastify__toast-body {
      color: ${({ theme }) => theme.darkGrey};
      
      .Toastify__toast-icon {
        & > svg {
          fill: ${({ theme }) => theme.red};
        }
      }
    }
  }

  .Toastify__progress-bar--error {
    background-color: ${({ theme }) => theme.red};
  }
`;
