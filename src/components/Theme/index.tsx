import { ReactNode } from 'react';
import { ThemeProvider } from 'styled-components';

interface ThemeProps {
  children: ReactNode;
}

const theme = {
  black: '#000000',
  lightDark: 'rgba(0, 0, 0, 0.6)',
  white: '#ffffff',
  darkGrey: '#232222',
  grey: '#5c5c5c',
  lightGrey: '#6c6c6c',
  lightestGrey: '#C4C4C4',
  green: '#03710E',
  red: '#9F0013',
  lightRed: '#b10013',
  shadow: '0 0 20px rgba(0, 0, 0, 0.25)',
  blackShadow: '5px 5px 20px rgba(0, 0, 0, 0.25)',
  redShadow: '0px 5px 20px #9F0013',
  cardShadow: '5px 5px 10px rgba(0, 0, 0, 0.25)',
  fieldShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
};

function Theme({ children }: ThemeProps) {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}

export type CustomThemeType = typeof theme;
export default Theme;
