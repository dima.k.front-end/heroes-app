import { DefaultProps } from 'types/common';
import { Wrap, Skeleton } from './styled';

interface SkeletonLoader extends DefaultProps<HTMLDivElement> {
  isLoading?: boolean;
}

export function SkeletonLoader({
  children,
  css,
  isLoading = false,
}: SkeletonLoader) {
  return (
    <Wrap css={css}>
      <Skeleton isLoading={isLoading} />
      {children}
    </Wrap>
  );
}
