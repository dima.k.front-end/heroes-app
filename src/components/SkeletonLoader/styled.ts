import styled, { css, keyframes } from 'styled-components';

const shimmer = keyframes`
  100% {
    transform: translateX(100%);
  }
`;

const Wrap = styled.div(
  ({ css: cssProp }) => css`
    position: relative;
    display: inline-block;
    width: 100%;
    height: 100%;
    overflow: hidden;
    ${cssProp};
  `
);

const Skeleton = styled.div<{
  isLoading: boolean;
}>(
  ({ isLoading, theme }) => css`
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    width: 100%;
    height: 100%;

    ${isLoading &&
    css`
      z-index: 0;
      background-color: ${theme.lightestGrey};

      &::after {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-image: linear-gradient(
          90deg,
          rgba(255, 255, 255, 0) 0,
          rgba(255, 255, 255, 0.2) 20%,
          rgba(255, 255, 255, 0.5) 60%,
          rgba(255, 255, 255, 0) 0
        );
        transform: translateX(-100%);
        animation: ${shimmer} 2s infinite;
        content: '';
      }
    `}
  `
);

export { Wrap, Skeleton };
