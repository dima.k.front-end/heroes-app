import styled from 'styled-components';

const RootInput = styled.input`
  padding: 10px;
  height: 40px;
  border: none;
  outline: none;
  box-shadow: ${({ theme }) => theme.fieldShadow};
  font-size: 14px;
  width: 100%;

  &::placeholder {
    color: ${({ theme }) => theme.lightDark};
  }
`;

export { RootInput };
