import { Link } from 'react-router-dom';

import { Loader } from 'components/Loader';
import { DefaultProps } from 'types/common';
import { Wrap } from './styled';

export type Variant = 'primary' | 'secondary';

interface ButtonProps extends DefaultProps<HTMLButtonElement> {
  variant?: Variant;
  href?: string;
  to?: string;
  fullWidth?: boolean;
  loading?: boolean;
}

export function Button({
  children,
  variant = 'primary',
  css,
  href,
  to,
  type,
  fullWidth = false,
  loading = false,
  onClick,
}: ButtonProps) {
  return (
    <Wrap
      type={type}
      as={href ? 'a' : to ? Link : 'button'}
      to={to}
      href={href}
      variant={variant}
      css={css}
      disabled={loading}
      onClick={!href || !to || !loading ? onClick : undefined}
      fullWidth={fullWidth}
    >
      {!loading ? children : <Loader isSmall />}
    </Wrap>
  );
}
