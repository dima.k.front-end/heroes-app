import styled, { css, DefaultTheme } from 'styled-components';

import { Variant } from 'components/Form/Button';

const getBgColor = (variant: Variant, theme: DefaultTheme) => {
  return variant === 'primary' ? theme.red : theme.grey;
};

const getHoverBgColor = (variant: Variant, theme: DefaultTheme) => {
  return variant === 'primary' ? theme.lightRed : theme.lightGrey;
};

const Wrap = styled.button<{
  variant: Variant;
  fullWidth?: boolean;
  disabled?: boolean;
}>`
  ${({ theme, variant, css: buttonCss, fullWidth, disabled }) => css`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 10px;
    padding: 0 5px;
    border: none;
    font-size: 14px;
    color: ${theme.white};
    height: 40px;
    width: ${fullWidth ? '100%' : 'auto'};
    text-transform: uppercase;
    cursor: ${disabled ? 'auto' : 'pointer'};
    transition: 0.3s;
    background-color: ${disabled
      ? theme.lightestGrey
      : getBgColor(variant, theme)};
    ${buttonCss}

    &:before,
    &:after {
      content: '';
      position: absolute;
      top: 0;
      bottom: 0;
      display: inline-block;
      transition: 0.3s;
    }

    &:before {
      right: 100%;
      border-right: 10px solid
        ${disabled ? theme.lightestGrey : getBgColor(variant, theme)};
      border-top: 10px solid transparent;
    }

    &:after {
      left: 100%;
      border-left: 10px solid
        ${disabled ? theme.lightestGrey : getBgColor(variant, theme)};
      border-bottom: 10px solid transparent;
    }

    ${!disabled &&
    css`
      &:hover {
        background-color: ${getHoverBgColor(variant, theme)};

        &:before {
          border-right-color: ${getHoverBgColor(variant, theme)};
        }

        &:after {
          border-left-color: ${getHoverBgColor(variant, theme)};
        }
      }
    `}
  `}
`;

export { Wrap };
