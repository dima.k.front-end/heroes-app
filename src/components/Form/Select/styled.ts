import styled, { css } from 'styled-components';

import { RootInput } from 'components/Form/shared/styled';

const Wrap = styled.div`
  position: relative;
`;

const FieldInput = styled(RootInput)`
  cursor: pointer;
  background-color: ${({ theme }) => theme.white};
`;

const OptionsWrap = styled.div`
  position: absolute;
  top: calc(100% + 2px);
  left: 0;
  width: 100%;
  height: auto;
  max-height: 166px;
  padding: 6px 4px 6px 0;
  background-color: ${({ theme }) => theme.white};
  box-shadow: ${({ theme }) => theme.shadow};
  border-radius: 6px;
  overflow-y: auto;
  z-index: 10;
`;

const Option = styled.div<{ active: boolean }>`
  padding: 8px;
  cursor: pointer;

  ${({ active }) =>
    active &&
    css`
      background-color: ${({ theme }) => theme.red};
      color: ${({ theme }) => theme.white};
    `}

  &:hover {
    background-color: ${({ theme }) => theme.lightRed};
    color: ${({ theme }) => theme.white};
  }
`;

export { Wrap, FieldInput, OptionsWrap, Option };
