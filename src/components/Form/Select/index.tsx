import { Controller } from 'react-hook-form';
import { useRef, useState, MouseEvent } from 'react';

import { EmptyList } from 'components/EmptyList';
import { useOutsideClick } from 'hooks/useOutsideClick';
import { DefaultProps, ISelectOption } from 'types/common';
import { FieldInput, Option, OptionsWrap, Wrap } from './styled';

interface SelectProps extends DefaultProps<HTMLInputElement> {
  options: ISelectOption[];
  name: string;
  control: any;
}

type ChangeFuncType = (value: string) => void;

export function Select({
  name,
  placeholder,
  options = [],
  control,
}: SelectProps) {
  const [dropdownIsOpened, setDropdownIsOpened] = useState(false);
  const ref = useRef(null);

  const onOpenDropdown = () => {
    setDropdownIsOpened((prevState) => !prevState);
  };

  const onChooseOption = (
    event: MouseEvent<HTMLDivElement>,
    option: ISelectOption,
    onChange: ChangeFuncType
  ) => {
    event.stopPropagation();
    onChange(option.value);
    setDropdownIsOpened(false);
  };

  const renderOptions = (value: string, onChange: ChangeFuncType) => {
    if (!options.length) return <EmptyList />;

    return options.map((option) => {
      return (
        <Option
          key={option.value}
          active={value === option.value}
          onClick={(event) => onChooseOption(event, option, onChange)}
        >
          {option.label}
        </Option>
      );
    });
  };

  useOutsideClick(ref, () => {
    if (dropdownIsOpened) {
      setDropdownIsOpened(false);
    }
  });

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { value, onChange } }) => (
        <Wrap
          ref={ref}
          onClick={onOpenDropdown}
        >
          <FieldInput
            type="text"
            value={
              options.find((option) => option.value === value)?.label ?? ''
            }
            placeholder={placeholder}
            readOnly
          />
          {dropdownIsOpened && (
            <OptionsWrap>{renderOptions(value, onChange)}</OptionsWrap>
          )}
        </Wrap>
      )}
    />
  );
}
