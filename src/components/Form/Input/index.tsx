import { Controller } from 'react-hook-form';

import { DefaultProps } from 'types/common';
import { Wrap, ErrorText, StyledInput } from './styled';

interface InputProps extends DefaultProps<HTMLInputElement> {
  name: string;
  control: any;
}

export function Input({ placeholder, name, control }: InputProps) {
  return (
    <Controller
      control={control}
      name={name}
      render={({ field: { value, onChange }, fieldState: { error } }) => (
        <Wrap>
          <StyledInput
            value={value}
            placeholder={placeholder}
            onChange={onChange}
          />
          {!!error && <ErrorText>{error?.message ?? ''}</ErrorText>}
        </Wrap>
      )}
    />
  );
}
