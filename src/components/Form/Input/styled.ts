import styled from 'styled-components';

import { RootInput } from '../shared/styled';

const Wrap = styled.div``;

const StyledInput = styled(RootInput)``;

const ErrorText = styled.div`
  color: ${({ theme }) => theme.red};
  font-size: 18px;
  font-weight: 700;
  margin-top: 25px;
`;

export { Wrap, StyledInput, ErrorText };
