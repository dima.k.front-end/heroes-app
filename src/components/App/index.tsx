import { Switch, Route, Redirect } from 'react-router-dom';

import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import CharactersPage from 'pages/CharactersPage';
import ComicsPage from 'pages/ComicsPage';
import CharacterPage from 'pages/CharacterPage';
import ComicPage from 'pages/ComicPage';
import NotFound from 'pages/NotFound';
import Bg from 'assets/bg-img.png';
import { BgImage, Main, Wrapper } from './styled';

function App() {
  return (
    <Wrapper>
      <Header />
      <Main>
        <Switch>
          <Route
            exact
            path="/characters"
            component={CharactersPage}
          />
          <Route
            path="/characters/:id"
            component={CharacterPage}
          />
          <Route
            exact
            path="/comics"
            component={ComicsPage}
          />
          <Route
            path="/comics/:id"
            component={ComicPage}
          />
          <Route
            exact
            path="/"
            render={() => <Redirect to="/characters" />}
          />
          <Route
            path="*"
            component={NotFound}
          />
        </Switch>
      </Main>
      <Footer />
      <BgImage src={Bg} />
    </Wrapper>
  );
}

export default App;
