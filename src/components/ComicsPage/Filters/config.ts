import yup from 'plugins/yup-config';

import { ISelectOption } from 'types/common';

export interface IComicsFilters {
  format: string;
  formatType: string;
  dateDescriptor: string;
}

const formatOptions: ISelectOption[] = [
  { label: 'Comic', value: 'comic' },
  { label: 'Magazine', value: 'magazine' },
  { label: 'Trade Paperback', value: 'trade paperback' },
  { label: 'Hardcover', value: 'hardcover' },
  { label: 'Digest', value: 'digest' },
  { label: 'Graphic Novel', value: 'graphic novel' },
  { label: 'Digital Comic', value: 'digital comic' },
  { label: 'Infinite Comic', value: 'Infinite Comic' },
];

const formatTypeOptions: ISelectOption[] = [
  { label: 'Comic', value: 'comic' },
  { label: 'Collection', value: 'collection' },
];

const dateDescriptorOptions: ISelectOption[] = [
  { label: 'Last Week', value: 'lastWeek' },
  { label: 'This Week', value: 'thisWeek' },
  { label: 'Next Week', value: 'nextWeek' },
  { label: 'This Month', value: 'thisMonth' },
];

const schema = yup.object().shape({
  format: yup.string(),
  formatType: yup.string(),
  dateDescriptor: yup.string(),
});

export { schema, formatOptions, formatTypeOptions, dateDescriptorOptions };
