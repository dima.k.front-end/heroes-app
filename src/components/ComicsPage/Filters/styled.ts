import styled from 'styled-components';

import { device } from 'utils/device';

const Wrap = styled.div`
  margin-top: 25px;

  @media ${device.md} {
    margin-top: 0;
    margin-bottom: 20px;
  }
`;

const Form = styled.form`
  display: grid;
  grid-template-columns: 168px 168px 168px 100px;
  grid-column-gap: 20px;
  grid-row-gap: 16px;

  @media ${device.md} {
    grid-template-columns: 1fr 1fr;
  }

  @media ${device.sm} {
    grid-template-columns: 1fr;
  }
`;

const Title = styled.div`
  font-weight: 700;
  font-size: 18px;
  margin-bottom: 10px;
`;

export { Wrap, Title, Form };
