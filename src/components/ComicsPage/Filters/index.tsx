import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect } from 'react';

import { Select } from 'components/Form/Select';
import { Button } from 'components/Form/Button';
import { useAppDispatch, useAppSelector } from 'hooks/redux-hook';
import { objWithoutEmptyProp } from 'utils/prepare';
import { comics } from 'store/comics';
import {
  schema,
  formatOptions,
  formatTypeOptions,
  dateDescriptorOptions,
  IComicsFilters,
} from './config';
import { Title, Wrap, Form } from './styled';

export function Filters() {
  const dispatch = useAppDispatch();
  const defaultValues = useAppSelector(comics.selectors.filters);

  const { handleSubmit, control } = useForm<IComicsFilters>({
    resolver: yupResolver(schema),
    defaultValues,
  });

  const onSubmit = (data: IComicsFilters) => {
    const fd = objWithoutEmptyProp(data);
    dispatch(comics.actions.SET_FILTERS(fd));
    dispatch(comics.thunks.getAllComics({ offset: 0, ...fd }));
  };

  useEffect(() => {
    return () => {
      dispatch(comics.actions.RESET_FILTERS());
    };
  }, []);

  return (
    <Wrap>
      <Title>Filters:</Title>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Select
          name="format"
          placeholder="Select format"
          options={formatOptions}
          control={control}
        />
        <Select
          name="formatType"
          placeholder="Select format type"
          options={formatTypeOptions}
          control={control}
        />
        <Select
          name="dateDescriptor"
          placeholder="Select date"
          options={dateDescriptorOptions}
          control={control}
        />
        <Button type="submit">Submit</Button>
      </Form>
    </Wrap>
  );
}
