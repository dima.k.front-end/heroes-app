import styled from 'styled-components';

import { CardHover, TextEllipsisForLines } from 'components/Theme/mixins';

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  height: 415px;
  font-size: 14px;
  font-weight: 700;
`;

const ImageWrap = styled.div`
  width: 100%;
  height: 350px;
  box-shadow: ${({ theme }) => theme.cardShadow};
  transition: 0.2s;
  ${CardHover};
`;

const Title = styled.div`
  margin-top: 10px;
  ${TextEllipsisForLines({ linesNumber: 2 })};
`;

const Price = styled.div`
  color: ${({ theme }) => theme.lightDark};
  text-transform: uppercase;
  margin-top: auto;
`;

export { Wrap, ImageWrap, Title, Price };
