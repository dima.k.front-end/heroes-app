import { Wrap, Spinner } from './styled';

interface LoaderProps {
  isSmall?: boolean;
}

export function Loader({ isSmall }: LoaderProps) {
  return (
    <Wrap isSmall={isSmall}>
      <Spinner />
    </Wrap>
  );
}
