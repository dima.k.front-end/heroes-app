import styled from 'styled-components';

const Wrap = styled.footer`
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  min-height: 50px;

  & > a {
    text-transform: uppercase;
    font-weight: 700;
    color: ${({ theme }) => theme.black};

    &:hover {
      color: ${({ theme }) => theme.lightDark};
    }
  }
`;

export { Wrap };
