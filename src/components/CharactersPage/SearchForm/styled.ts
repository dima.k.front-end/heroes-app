import styled from 'styled-components';

import { device } from 'utils/device';

const Form = styled.form`
  padding: 25px;
  min-height: 180px;
  margin-top: 30px;
  box-shadow: ${({ theme }) => theme.shadow};
  background-color: ${({ theme }) => theme.white};

  @media ${device.lg} {
    margin-bottom: 30px;
  }
`;

const Title = styled.div`
  font-size: 18px;
  font-weight: 700;
  margin-bottom: 15px;
`;

const Wrap = styled.div`
  display: grid;
  grid-template-columns: 1fr 0.5fr;
  column-gap: 24px;
`;

const HelperText = styled.div<{ error: boolean }>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  column-gap: 30px;
  color: ${({ error, theme }) => (error ? theme.red : theme.green)};
  margin-top: 25px;
  font-weight: 700;
  font-size: 18px;
  min-height: 40px;
`;

export { Form, Title, Wrap, HelperText };
