import yup from 'plugins/yup-config';

const defaultValues = {
  name: '',
};

const schema = yup.object().shape({
  name: yup.string().required(),
});

export { defaultValues, schema };
