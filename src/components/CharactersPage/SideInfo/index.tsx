import { Button } from 'components/Form/Button';
import { Image } from 'components/Image';
import { Loader } from 'components/Loader';
import { useAppSelector } from 'hooks/redux-hook';
import { Status } from 'utils/const';
import { character } from 'store/characters/character';
import {
  Wrap,
  EmptyText,
  Header,
  ImageWrap,
  NameBlock,
  Name,
  ButtonBlock,
  Description,
} from './styled';

export function SideInfo() {
  const data = useAppSelector(character.selectors.character);
  const fetchingStatus = useAppSelector(
    character.selectors.characterFetchingStatus
  );

  const isEmpty = !data && fetchingStatus === Status.IDLE;
  const loading = fetchingStatus === Status.PENDING;

  if (isEmpty) {
    return (
      <Wrap>
        <EmptyText>Please select a character to see information</EmptyText>
      </Wrap>
    );
  }

  if (loading) {
    return (
      <Wrap>
        <Loader />
      </Wrap>
    );
  }

  if (data) {
    return (
      <Wrap>
        <Header>
          <ImageWrap>
            <Image
              src={data.imageHref}
              alt={data.name}
            />
          </ImageWrap>
          <NameBlock>
            <Name>{data.name}</Name>
            <ButtonBlock>
              <Button to={`/characters/${data.id}`}>Homepage</Button>
              <Button
                variant="secondary"
                href={data.wikiUrl}
              >
                Wiki
              </Button>
            </ButtonBlock>
          </NameBlock>
        </Header>
        <Description>{data.description}</Description>
      </Wrap>
    );
  }

  return null;
}
