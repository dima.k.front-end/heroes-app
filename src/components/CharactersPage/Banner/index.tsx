import { useEffect } from 'react';

import { Button } from 'components/Form/Button';
import { Image } from 'components/Image';
import { Loader } from 'components/Loader';
import WeaponsIconSrc from 'assets/weapons.png';
import { useAppDispatch, useAppSelector } from 'hooks/redux-hook';
import { DefaultProps } from 'types/common';
import { useMediaQuery } from 'hooks/useMediaQuery';
import { device } from 'utils/device';
import { Status } from 'utils/const';
import { character } from 'store/characters/character';
import {
  Wrap,
  LeftSide,
  RightSide,
  WeaponsIcon,
  Title,
  ImageWrap,
  Name,
  Description,
  ButtonsBlock,
  InfoWrap,
  NotFound,
} from './styled';

export function Banner({ css }: DefaultProps<HTMLButtonElement>) {
  const dispatch = useAppDispatch();
  const data = useAppSelector(character.selectors.randomCharacter);
  const fetchingStatus = useAppSelector(
    character.selectors.randomFetchingStatus
  );

  const isMobileDevice = useMediaQuery(device.sm);

  const getRandomCharacter = () => {
    const id = Math.floor(Math.random() * (1011400 - 1011000) + 1011000);
    dispatch(character.thunks.getRandomCharacter(id));
  };

  useEffect(() => {
    getRandomCharacter();
  }, []);

  const leftSide =
    fetchingStatus === Status.PENDING ? (
      <Loader />
    ) : !data ? (
      <NotFound>Character not found</NotFound>
    ) : (
      <>
        <ImageWrap>
          <Image
            src={data.imageHref}
            alt={data.name}
          />
        </ImageWrap>
        <InfoWrap>
          <Name>{data.name}</Name>
          <Description>{data.description}</Description>
          <ButtonsBlock>
            <Button
              to={`/characters/${data.id}`}
              fullWidth={!isMobileDevice}
            >
              Homepage
            </Button>
            <Button
              variant="secondary"
              href={data.wikiUrl}
              fullWidth={!isMobileDevice}
            >
              Wiki
            </Button>
          </ButtonsBlock>
        </InfoWrap>
      </>
    );

  return (
    <Wrap css={css}>
      <LeftSide>{leftSide}</LeftSide>
      <RightSide>
        <Title>
          Random character for today!
          <br /> Do you want to get to know him better?
        </Title>
        <div>Or choose another one</div>
        <Button
          css={{ minWidth: '101px', marginTop: '12px' }}
          onClick={getRandomCharacter}
        >
          Try it
        </Button>
        <WeaponsIcon
          src={WeaponsIconSrc}
          alt="Weapons"
        />
      </RightSide>
    </Wrap>
  );
}
