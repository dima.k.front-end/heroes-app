import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import store, { RootState } from 'store';

type AppDispatch = typeof store.dispatch;
const useAppDispatch: () => AppDispatch = useDispatch;
const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export { useAppDispatch, useAppSelector };
